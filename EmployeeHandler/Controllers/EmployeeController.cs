﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using System.Data;
using System.Data.Entity;
using EmployeeHandler.Models;

namespace EmployeeHandler.Controllers
{
    public class EmployeeController : Controller
    {
        private EmployeeContext context=new EmployeeContext();
        private Converter converter = new Converter();
        
        private string fileName;
        
        //
        // GET: /Employee/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FilterEmployees(string column, string term)
        {
            IList<Employee> emps=null;
            if (column.Equals("Name")) { emps=context.Employees.Where(e=>e.Name.Contains(term)).ToList<Employee>(); }
            else if (column.Equals("DateOfBirth")) { emps = context.Employees.AsEnumerable().Where(e => e.DateOfBirth.ToString().Contains(term)).ToList<Employee>(); }
            else if (column.Equals("Married"))
            {
                if (!term.Equals("True", StringComparison.InvariantCultureIgnoreCase) && !term.Equals("False", StringComparison.InvariantCultureIgnoreCase)) emps = null; //if (term!= "True" && term!= "False") emps=null;
                else emps = context.Employees.AsEnumerable().Where(e => e.Married.Equals(Convert.ToBoolean(term))).ToList<Employee>(); }
            else if (column.Equals("Phone")) { emps = context.Employees.Where(e => e.Phone.Contains(term)).ToList<Employee>(); }
            else if (column.Equals("Salary")) { emps = context.Employees.AsEnumerable().Where(e => e.Salary.CompareTo(Convert.ToDecimal(term))>0).ToList<Employee>(); }
            //return context.Employees.Where(e =>e.GetType().GetProperty(column).GetValue().Equals(term)).;
            Debug.WriteLine("22"+emps);
            if (emps == null) return new HttpStatusCodeResult(404, "Bad filter chosen");
            return PartialView("_GetAll", emps);
        }

        public ActionResult SortEmployees(string column)
        {
            IList<Employee> emps = null;
            if (column.Equals("Name")) {emps = context.Employees.AsEnumerable().OrderBy(e => e.Name).ToList<Employee>(); }
            else if (column.Equals("DateOfBirth")) {emps = context.Employees.AsEnumerable().OrderBy(e => e.DateOfBirth).ToList<Employee>(); }
            else if (column.Equals("Married")) {emps = context.Employees.AsEnumerable().OrderBy(e => e.Married).ToList<Employee>(); }
            else if (column.Equals("Phone")) {emps = context.Employees.AsEnumerable().OrderBy(e => e.Phone).ToList<Employee>(); }
            else if (column.Equals("Salary")) {emps = context.Employees.AsEnumerable().OrderBy(e => e.Salary).ToList<Employee>(); }
            else if (column.Equals("NoSorting")) {emps = context.Employees.ToList<Employee>(); }
            //List<Employee> emps = context.Employees.AsEnumerable().OrderBy(e=>e.GetType().GetProperty(column)).ToList<Employee>();
            Debug.WriteLine("33" + emps[0].EmployeeId + "-" + emps[1].EmployeeId + "-" + emps[2].EmployeeId);
            return PartialView("_GetAll", emps);
        }

        public ActionResult GetAll()
        {
            var list = context.Employees.ToList();
            return PartialView("_GetAll", list);
        }

        public IList<Employee> GetEmployees()
        {
            return context.Employees.ToList();
        }

        public void AddEmployee(Employee emp)
        {
            context.Employees.Add(emp);
            context.SaveChanges();
        }

        [HttpPost]
        public ActionResult Upload()
        {
            HttpPostedFileBase fileBase=null;
            try
             {
                fileBase = Request.Files[0];
       
            }
            catch { return Content("No file chosen"); }

            if (fileBase != null && fileBase.ContentLength > 0)
            {
                fileName = fileBase.FileName;
            }

            try
            {
                IList<Employee> emps = converter.convertFromCSV(fileBase);
                foreach (Employee e in emps)
                {
                    AddEmployee(e);
                }
            }
            catch { return Content("Conversion error"); }

            return Content("File " + fileName + " uploaded");
        }
        
        [HttpPost]     
        public ActionResult Edit(EmployeeDTO newEmployee)
        {
            Employee e = context.Employees.Find(newEmployee.EmployeeId);
            e.Name = newEmployee.Name;
            e.DateOfBirth = Convert.ToDateTime(newEmployee.DateOfBirth).Date;
            e.Married = newEmployee.Married;
            e.Phone=newEmployee.Phone;
            e.Salary=Convert.ToDecimal(newEmployee.Salary);
            try
            {
                context.Entry(e).State = EntityState.Modified;
                context.SaveChanges();
                return PartialView("_GetAll", GetEmployees());
            }
            catch (Exception ex) { 
                return Content("There is no such employee");
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Employee e = context.Employees.Find(id);
            try
            {
                context.Employees.Remove(e);
                context.SaveChanges();
                return PartialView("_GetAll", GetEmployees());
            }
            catch (Exception ex)
            {
                return Content("There is no such employee");
            }
        }

    }
}
