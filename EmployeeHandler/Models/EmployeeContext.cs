﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace EmployeeHandler.Models
{
    public class EmployeeContext: DbContext
    {
        public EmployeeContext()
            : base("employeesList")
        {
        }
        public DbSet<Employee> Employees { get; set; }

    }
}