﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EmployeeHandler.Models
{
    public class EmployeeDTO
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        //[DataType(DataType.Date)]
        public string DateOfBirth { get; set; }
        public Boolean Married { get; set; }
        public string Phone { get; set; }
        public string Salary { get; set; }
    }
}